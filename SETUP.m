clc;
clear;

%==== HORIZONTE DE PREDICCION =====
                                  %
HORIZONTE_PREDICCION = 4;         %
                                  %
%==================================
currentFOLDER = pwd;

% qpOASES

qpoasesINTERFACE = '\qpoases\interfaces\matlab';
qpoasesFOLDER = strcat(pwd,qpoasesINTERFACE);

cd(qpoasesFOLDER)
mex -setup C++
make
copyfile('qpOASES_sequence.mex*', currentFOLDER)
copyfile('qpOASES.mex*',currentFOLDER)

% OSQP

cd(currentFOLDER)
cd osqp-matlab
mex -setup C++
make_osqp
copyfile('osqp.m', currentFOLDER)
copyfile('osqp_mex.mex*',currentFOLDER)

cd(currentFOLDER)

% FiOrdOS

fiordosFOLDER = strcat(currentFOLDER,'\fiordos');
cd(fiordosFOLDER)
mkdir tbxmanager
cd tbxmanager
urlwrite('http://www.tbxmanager.com/tbxmanager.m', 'tbxmanager.m');
tbxmanager
savepath
tbxmanager install fiordos yalmip sedumi
cd ..
python_command_format = 'python SETUP_fiordos.py %d'
python_command = sprintf(python_command_format, HORIZONTE_PREDICCION)
if isfile('fiordos_mex.c')
    delete fiordos*
end
system(python_command)
fiordos
copyfile('fiordos_mex.mex*',currentFOLDER)
cd ..