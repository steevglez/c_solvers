import sys

def set_formatter(dim_H):
    essbox = []
    simpleset = 'X = SimpleSet('
    for i in range (1,dim_H+1):
        essbox.append("X{} = EssBox(1, 'l',[-999], 'u',[999], 'rot','param');".format(str(i)))
        simpleset = simpleset + "X{},".format(str(i))
    simpleset = simpleset[:-1]
    simpleset = simpleset + ");"
    return [simpleset, essbox]

dim_H = int(sys.argv[1])
dim_Mx = 6*dim_H
        
nombre_archivo_m = 'fiordos.m'
simpleset, essbox = set_formatter(dim_H)
    
plantilla_m = open('plantilla_fiordos.m', 'r')
mfile = open(nombre_archivo_m, 'w')
        
while(True):
    line_plant = plantilla_m.readline()
    if not line_plant:
        break
        
    if('$' in line_plant):
        for i in range(0,len(essbox)):
            ess_string = str(essbox[i])
            mfile.write(line_plant.replace('$',ess_string))
        simpleset_string = str(simpleset) + '\n'
        mfile.write(simpleset_string)
    elif('&' in line_plant):
        mfile.write(line_plant.replace('&',str(dim_Mx)))   
    else:
        mfile.write(line_plant)
plantilla_m.close()
mfile.close()
print('Archivo: {} generado'.format(nombre_archivo_m))
